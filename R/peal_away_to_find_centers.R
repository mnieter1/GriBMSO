#' peal away until we hit the center skeleton
#'
#' @param a_1D_index_list the list of 1D-IDs specifying the currently filled cubicles
#' @param theGridMapSpecs the specification of the grid map paraneters
#' @param peal_count_range_for_neighborhood_count defining fully surrounded and when to stop; e.g. what is a line 2D -> c(3, 9)
#'
#' @return the index list of the skeleton
#' @export
#'
#' @examples
peal_away_to_find_centers <- function(a_1D_index_list = those_cubicles_were_filled,
                                      theGridMapSpecs = testGrid3D, peal_count_range_for_neighborhood_count = c(3, 27)){

  # we have to peal away one layer after another
  # outer cubicles are defined as having empty cubicles as neighbors
  # thus pealing can be done as long as there are enough neighbors to define a structure like line

  # define a collector for IDs to keep
  collecting_passed_IDs <- NA
  # define a counter specifying the number to be removed
  did_we_remove_something_lately <- TRUE
  how_many_will_we_keep <- 1 # if one calls this function it should theoretically keep at least one cubicle
  # as long as there were things to be removed before or at the first time checking,
  # keep on checking if more stuff can be removed
  while (did_we_remove_something_lately) {

    # to make saving fast we initialize a vector to store the keep list
    keep_list <- rep(NA, length(a_1D_index_list))

    # reset checker
    how_many_will_we_keep <- 1
    current_keep_counter <- 0
    # reset the flag in the loop to see if we find something to remove or can leave the loop this time
    did_we_remove_something_lately <- FALSE

    # so now for each cubicle get the neighborhood and check whether its on the outside at all;
    # if not just keep the cubicle
    # if outside detected, check for connectedness using the peal_count_range_for_neighborhood_count value,
    # e.g. a 3 should return in a 3D-space a simple line of cubicles in the end
    for(index_to_check in 1:length(a_1D_index_list)){
      # get the neighborhood
      current_lookup <- lookup_neighborhood_1D_indices(a_1D_ID = a_1D_index_list[index_to_check], theGridMapSpecs = theGridMapSpecs)
      # to check if in a border region of the plot box check the theoretical count of neighbor indices
      theoretical_number_which_could_be_filled <- length(na.exclude(current_lookup))
      # percentage of cubicles theoretically reachable
      accessabliity_score <- theoretical_number_which_could_be_filled/length(current_lookup)

      # find the ones which are actually filled
      current_lookup_neighborhood_actually_filled <- a_1D_index_list[a_1D_index_list %in% na.exclude(current_lookup)]

      # remove the NAs and return the number of potentially filled neighbors
      current_lookup_neighborhood_filled_counter <- length(current_lookup_neighborhood_actually_filled)


      # check if at least one empty in 3D means fewer than 27 and to be removed it should contain more than the cutoff
      if(current_lookup_neighborhood_filled_counter > peal_count_range_for_neighborhood_count[1] &
         current_lookup_neighborhood_filled_counter < peal_count_range_for_neighborhood_count[2] &
         current_lookup_neighborhood_filled_counter < theoretical_number_which_could_be_filled){
        # if true this can be removed
        # actually we just do not copy it any more :-)
        # but we set the flag to know about the removal
        did_we_remove_something_lately <- TRUE
      }else{
        # one was kept so update counter
        current_keep_counter <- current_keep_counter + 1
        # we have to keep this
        # so let's make a copy for the next round in the keep_list
        keep_list[current_keep_counter] <- a_1D_index_list[index_to_check]
      }

    }

    if(current_keep_counter>0){
      how_many_will_we_keep <- current_keep_counter
      print(paste("we kept in this round of pealing" , how_many_will_we_keep, "cubicles"))
    }
    # now overwrite the old list with the ones we keep now
    a_1D_index_list <- keep_list[1:current_keep_counter]

  }


  return(a_1D_index_list)
}
