#' draw the boxes for the cubicles in a 2D plot
#'
#' @param a_1D_index the indices representing the filled cubicles
#' @param theGridMapSpecs the specification of the grid map
#' @param col the color for the box
#' @param border the color for the border of the box
#'
#' @return the current plot modified with inserted boxes
#' @export
#'
#' @examples
draw_the_boxes_for_cubicles <- function(a_1D_index = 1,
                                        theGridMapSpecs, col = "red", border = "blue"){
  # now let's draw them to the plot frame
  for (cubicle_index in 1: length(a_1D_index)) {
    lower_and_upper_corner_vector = take_1D_index_return_lower_upper_corner_vector_for_2D(a_1D_index = a_1D_index[cubicle_index],
                                                                                          theGridMapSpecs = theGridMapSpecs)

    rect(xleft = lower_and_upper_corner_vector[1],
         ybottom = lower_and_upper_corner_vector[2],
         xright = lower_and_upper_corner_vector[3],
         ytop = lower_and_upper_corner_vector[4],
         col = col,
         border = border) # coloured
  }

}
