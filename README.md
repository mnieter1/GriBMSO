This is the GriBMSO R package, which is a port from a previous Java Package for 'grid-based mapping and set operations'.

Roughly described one can drop data onto an n-dimensional cubicle space and map the data this way to a grid space representation.
A given layout will result in a deterministic indexing schema (like a spacial database index system).

Have a look at the vignette folder to see an animated gif exlaining the process of mapping.
vignettes/GriBMSO_iris.gif

How to use:
Given three testsets, e.g. A <- positives, B <- negatives, C <- query 
One drops the datasets each onto the same grid layout and receives for each an indices list were the data was placed, which is equivalent to mapping it in the nD space
Only filled cublicles get 'created' indexed.
If one now uses these indice lists, one can quickly filter for overlaps of (A AND B), exclusive in (A NOT in B) or in B (B NOT in A).
Finally one can assign to the query C the three corresponding class defined before using just A and B.
Or in case of newly occuring exclusive C (C NOT in A OR B) one detects this, due to not mapping to known used indices.




Implementation
The package contains a GridMap S4 class to be used for specifying the parameters/layout defining the grid map space.
e.g. nr of divisions per dimension, the min max for each dimension.

Furthermore there are more functions to use this with e.g.:
dropping data onto grid map and receiving cubicle indices
drawing the filled cubicles in 2D as well 3D

for working with microscopy data there were more functions added specializing in voxel space manipulation
interpolating missing layers
pealing of cubicle clouds to find e.g. the center points to define the blood vessel center path.

