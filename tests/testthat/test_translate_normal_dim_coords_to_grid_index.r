context("bin drop position to 1D-ID representation")

test_that("1D projection", {

  # make a 3D grid spec
testGrid <- initializeGridMap(min_of_x_y_etc = c(0, 0, 0), max_of_x_y_etc = c(100, 100, 100), nr_of_divisions = c(4, 4, 4))

  # cubicle 15
  expect_equal(translate_normal_dim_coords_to_grid_index(x_y_etc = c(3, 4, 1), theGridMapSpecs = testGrid), 15)
  # cubicle 31
  expect_equal(translate_normal_dim_coords_to_grid_index(x_y_etc = c(3, 4, 2), theGridMapSpecs = testGrid), 31)
  # cubicle 47
  expect_equal(translate_normal_dim_coords_to_grid_index(x_y_etc = c(3, 4, 3), theGridMapSpecs = testGrid), 47)
  # cubicle 63
  expect_equal(translate_normal_dim_coords_to_grid_index(x_y_etc = c(3, 4, 4), theGridMapSpecs = testGrid), 63)
  # cubicle 64 last one
  expect_equal(translate_normal_dim_coords_to_grid_index(x_y_etc = c(4, 4, 4), theGridMapSpecs = testGrid), 64)



  # to small return NA
  expect_equal(translate_normal_dim_coords_to_grid_index(x_y_etc = c(-4, 4, 4), theGridMapSpecs = testGrid), NA)
  # to big return NA
  expect_equal(translate_normal_dim_coords_to_grid_index(x_y_etc = c(4, 4, 5), theGridMapSpecs = testGrid), NA)
  # contains NAs
  expect_equal(translate_normal_dim_coords_to_grid_index(x_y_etc = c(NA, 4, 2), theGridMapSpecs = testGrid), NA)


})

