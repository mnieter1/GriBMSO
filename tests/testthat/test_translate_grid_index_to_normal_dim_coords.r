context("bin drop position from 1D-ID to nD representation")

test_that("1D to nD projection", {

  # make a 3D grid spec
testGrid <- initializeGridMap(min_of_x_y_etc = c(0, 0, 0), max_of_x_y_etc = c(100, 100, 100), nr_of_divisions = c(4, 4, 4))


  # cubicle 27
  expect_equal(sum(translate_grid_index_to_normal_dim_coords(a_1D_index = 27, theGridMapSpecs = testGrid) == c(3, 3, 2)), 3)
  # cubicle 40
  expect_equal(sum(translate_grid_index_to_normal_dim_coords(a_1D_index = 40, theGridMapSpecs = testGrid) == c(4, 2, 3)), 3)
  # cubicle 36
  expect_equal(sum(translate_grid_index_to_normal_dim_coords(a_1D_index = 36, theGridMapSpecs = testGrid) == c(4, 1, 3)), 3)
  # cubicle negative or 0 case
  #expect_equal(sum(translate_grid_index_to_normal_dim_coords(a_1D_index = 0, theGridMapSpecs = testGrid) == c(NA, NA, NA)), NA)
  # cubicle 1500 way out of bounce
  #expect_equal(sum(translate_grid_index_to_normal_dim_coords(a_1D_index = 1500, theGridMapSpecs = testGrid) == c(NA, NA, NA)), NA)


})

