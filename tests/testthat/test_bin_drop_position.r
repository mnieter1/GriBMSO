context("bin drop position")

test_that("bin drop position function", {

  # second bin
  expect_equal(bin_drop_position(10, 0, 100, 10), 2)
  # first bin
  expect_equal(bin_drop_position(0, 0, 100, 10), 1)
  # last bin
  expect_equal(bin_drop_position(100, 0, 100, 10), 10)
  # to small return NA
  expect_equal(bin_drop_position(-1, 0, 100, 10), NA)
  # to big return NA
  expect_equal(bin_drop_position(101, 0, 100, 10), NA)


})
